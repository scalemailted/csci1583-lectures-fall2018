public class Dice
{
	public static int rollD6()
	{
		int die = (int)(Math.random() * 6) + 1;
		return die;
	}

	public static int roll2D6()
	{
		int die1 = (int)(Math.random() * 6) + 1;
		int die2 = (int)(Math.random() * 6) + 1;
		System.out.printf("%d+%d=%d\n",die1,die2, die1+die2);
		return die1 + die2;
	}
}