public class Craps
{
	//Constants used by game rules
	private static final int SNAKE_EYES = 2;
	private static final int TREY = 3;
	private static final int SEVEN = 7;
	private static final int YO = 11;
	private static final int BOXCAR = 12;

	private static Status gameStatus;
	private static int myPoint;

	public static void main(String[] args)
	{
		firstRound();
		while (gameStatus == Status.CONTINUE)
		{
			nextRound();
		}
		printResults();
	}

	public static void firstRound()
	{
		int dieSum = Dice.roll2D6();
		if (dieSum == SEVEN || dieSum == YO )
		{
			gameStatus = Status.WIN;
		}
		else if (dieSum == SNAKE_EYES || dieSum == TREY || dieSum == BOXCAR)
		{
			gameStatus = Status.LOSE;
		}
		else
		{
			gameStatus = Status.CONTINUE;
			myPoint = dieSum;
		}
	}

	public static void nextRound()
	{
		int dieSum = Dice.roll2D6();
		if (dieSum == SEVEN)
		{
			gameStatus = Status.LOSE;
		}
		else if (dieSum == myPoint)
		{
			gameStatus = Status.WIN;
		}
	}

	public static void printResults()
	{
		if (gameStatus == Status.WIN)
		{
			System.out.println("You won!");
		}
		else
		{
			System.out.println("You lost!");
		}
	}


}