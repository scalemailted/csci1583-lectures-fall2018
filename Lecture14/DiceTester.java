public class DiceTester
{
	public static void main(String[] args)
	{
		int counter1 = 0;
		int counter2 = 0;
		int counter3 = 0;
		int counter4 = 0;
		int counter5 = 0;
		int counter6 = 0;

		for (int i=0; i<6_000_000; i++)
		{
			int die = Dice.rollD6();
			if (die == 1)
			{
				counter1++;
			}
			else if (die == 2)
			{
				counter2++;
			}
			else if (die == 3)
			{
				counter3++;
			}
			else if (die == 4)
			{
				counter4++;
			}
			else if (die == 5)
			{
				counter5++;
			}
			else if (die == 6)
			{
				counter6++;
			}
		}
		System.out.printf("1: %d\n", counter1);
		System.out.printf("2: %d\n", counter2);
		System.out.printf("3: %d\n", counter3);
		System.out.printf("4: %d\n", counter4);
		System.out.printf("5: %d\n", counter5);
		System.out.printf("6: %d\n", counter6);	
	}
}