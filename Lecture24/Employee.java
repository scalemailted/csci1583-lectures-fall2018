public class Employee
{
	private String name;
	private Date hireDate;

	public Employee(String name, Date hire)
	{
		this.name = name;
		this.hireDate = hire;
	}

	public Employee(Employee e)
	{
		this.name = e.getName();
		this.hireDate = e.getHireDate();
	}

	//getter
	public String getName()
	{
		return this.name;
	}

	public Date getHireDate()
	{
		return this.hireDate;
	}

	//setter
	public void setName(String name)
	{
		this.name = name;
	}

	public void setHireDate(Date hire)
	{
		this.hireDate = hire;
	}

	public String toString()
	{
		return String.format("%s, hired: %s",this.name, this.hireDate);
	}


}