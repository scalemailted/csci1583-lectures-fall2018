public class CommissionEmployee extends Employee
{
	private double rate;
	private double sales;

	public CommissionEmployee(String name, Date hire, double rate, double sales)
	{
		super(name, hire);
		this.rate = rate;
		this.sales = sales;
	}

	public String toString()
	{
		String employee = super.toString();
		return String.format("[Commission] %s rate: %.02f sales:$%.02f",employee, this.rate, this.sales);
	}
}