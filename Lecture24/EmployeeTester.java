public class EmployeeTester
{
	public static void main(String[] args)
	{
		/*Employee tim = new Employee("Tim", new Date(31,12,1999) );
		Date toddHireDate = new Date(6,6,1966);
		Employee todd = new Employee("Todd",  toddHireDate);
		Employee tbird = tim;

		System.out.println(tim);
		System.out.println(todd);
		System.out.println(tbird);

		tbird.setName("Tbird");
		System.out.println(tim);

		Employee tom = new Employee(tim);
		tom.setName("tom");
		tom.setHireDate(new Date(6,6,2100));
		System.out.println(tom);
		System.out.println(tim);*/

		SalaryEmployee tim = new SalaryEmployee("Tim", new Date(31,12,1999), 100000 );
		System.out.println(tim);
		CommissionEmployee john = new CommissionEmployee("John", new Date(1,1,2017), 0.10, 10000);
		System.out.println(john);
		HourlyEmployee jean = new HourlyEmployee("Jean", new Date(2,2,2010), 40.25, 24.5 );
		System.out.println(jean);
	}
}