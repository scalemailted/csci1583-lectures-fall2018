import java.util.Scanner;

public class Switch
{
	public static void main(String[] args)
	{
		//Scanner for input
		Scanner input = new Scanner(System.in);
		//left operand
		int lefthand;
		//right operand
		int righthand;
		//operator
		String operator;
		//hold the result
		int result;

		//prompt to user for a expression
		System.out.print("Enter a mathematical expression separated by spaces: ");
		//get the lefthand operand
		lefthand = input.nextInt();
		//get operator
		operator = input.next();
		//get the righthand operand
		righthand = input.nextInt();

		switch(operator)
		{
			case "+":
			{
				result = lefthand + righthand;
				System.out.printf("%d%s%d=%d\n",lefthand,operator,righthand,result);
			}
			break;
			case "-":
			{
				result = lefthand - righthand;
				System.out.printf("%d%s%d=%d\n",lefthand,operator,righthand,result);
			}
			break;
			case "x":
			case "X":
			case "*":
			{
				result = lefthand * righthand;
				System.out.printf("%d%s%d=%d\n",lefthand,operator,righthand,result);
			}
			break;
			case "/":
			{
				result = lefthand / righthand;
				System.out.printf("%d%s%d=%d\n",lefthand,operator,righthand,result);
			}
			break;
			default:
			{
				System.out.println("Not a valid operator");
			}
		}

	}
}