public class Methods
{
	//Method: No inputs, no outputs
	public static void print10x()
	{
		for(int i=0; i<10; i++)
		{
			System.out.printf("[%02d] Hello World\n", i);
		}
	}

	//Method: Input, no output
	public static void print10x(String name)
	{
		for (int i=0; i<10; i++)
		{
			System.out.printf("[%02d] Hello %s\n", i, name);
		}
	}

	//Method: No input, output
	public static int fiveGetter()
	{
		int five = 5;
		return five;
	}

	//Method: input, output
	public static int quarterBack(double money)
	{
		int quarters = (int)(money/0.25);
		return quarters;
	}


}











