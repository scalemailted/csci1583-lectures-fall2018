public class Storage
{
	public static void main(String[] args)
	{
		int intNumber; //declaration
		intNumber = 3; //assignment
		System.out.printf("intNumber is: %d\n",intNumber);
		int intNumber2 = 4; //initialization
		intNumber = 34;
		System.out.printf("intNumber is: %d\n", intNumber);

		boolean booleanVariable = false; 
		System.out.printf("booleanVariable: %b\n", booleanVariable);

		double fractionalData = 3.14;
		System.out.printf("fractionalData: %f", fractionalData);

	}
}