public class TimeTester
{
	public static void main(String[] args)
	{
		Time t1 = new Time(12,1,1);
		System.out.println( t1.toUniversal() );
		System.out.println( t1.toString() );

		Time t2 = new Time(0,1,1);
		System.out.println( t2.toUniversal() );
		System.out.println( t2.toString() );

		Time t3 = new Time(13,1,1);
		System.out.println( t3.toUniversal() );
		System.out.println( t3.toString() );

		Time t4 = new Time(3,1,1);
		System.out.println( t4.toUniversal() );
		System.out.println( t4.toString() );


		Time t5;
		try
		{
			t5 = new Time();
		}
		catch(IllegalArgumentException e)
		{
			System.out.println(e);
			t5 = new Time(13,0,0);
		}
		System.out.println( t5.toUniversal() );
		System.out.println( t5.toString() );
	}
}