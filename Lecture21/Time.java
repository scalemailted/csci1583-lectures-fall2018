public class Time
{
	private int hours;
	private int minutes;
	private int seconds;

	public Time(int hours, int minutes, int seconds)
	{
		this.setTime(hours, minutes, seconds);
	}

	public Time(int hours, int minutes)
	{
		this(hours, minutes, 0);
	}

	public Time(int hours)
	{
		this(hours, 0);
	}

	public Time()
	{
		this(0);
	}

	public void setTime(int hours, int minutes, int seconds)
	{
		this.setHour(hours);
		this.setMinutes(minutes);
		this.setSeconds(seconds);
	}

	private void setHour(int hours)
	{
		if ( hours < 0 || hours > 23 )
		{
			throw new IllegalArgumentException("hours must be between 0-23");
		}
		this.hours = hours;
	}

	private void setMinutes(int minutes)
	{
		if ( minutes < 0 || minutes > 59)
		{
			throw new IllegalArgumentException("minutes must be between 0-59");
		}
		this.minutes = minutes;
	}

	private void setSeconds(int seconds)
	{
		if (seconds < 0 || seconds > 59)
		{
			throw new IllegalArgumentException("seconds must be between 0-59");
		}
		this.seconds = seconds;
	}




	public String toUniversal()
	{
		return String.format("%02d:%02d:%02d", 
							this.hours, 
							this.minutes, 
							this.seconds);
	}

	public String toString()
	{
		return String.format("%02d:%02d:%02d %s", 
							(this.hours % 12 == 0) ? 12 : this.hours % 12, 
							this.minutes, 
							this.seconds,
							(this.hours >= 12) ? "PM" : "AM"  );
	}
}