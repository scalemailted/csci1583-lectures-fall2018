public class MultiDim
{
	public static void main(String[] args)
	{
		String[][] names = 
		{
			{"Ted", "Alex", "Joe"},
			{"Mary", "Jo", "Lily", "Tim"},
			{"Mike", "Ed"}
		};

		for(int i=0; i<names.length; i++)
		{
			for(int j=0; j<names[i].length; j++)
			{
				System.out.printf("names[%d][%d] = %s\n",i,j,names[i][j]);
			}
		}
	}
}