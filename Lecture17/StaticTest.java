public class StaticTest
{
	private static int staticCounter = 0;
	private int instanceCounter = 0;

	public static void main(String[] args)
	{
		System.out.println("staticCounter: "+staticCounter);
		StaticTest test = new StaticTest();
		System.out.println("instanceCounter: "+test.instanceCounter);
	}
}