public class Dice
{
	public static int rollD6()
	{
		int die = (int)(Math.random() * 6) + 1;
		return die;
	}

	public static int rollD6(int qty)
	{
		int sum = 0;
		for (int i=0; i<qty ; i++)
		{
			sum += (int)(Math.random() * 6) + 1;
		}
		return sum;
	}

	public static int roll2D6()
	{
		int die1 = (int)(Math.random() * 6) + 1;
		int die2 = (int)(Math.random() * 6) + 1;
		//System.out.printf("%d+%d=%d\n",die1,die2, die1+die2);
		return die1 + die2;
	}

	public static int rollDice(int... diceArray)
	{
		int dieSum = 0;
		for (int die : diceArray)
		{
			int dieValue = (int)(Math.random() * die) + 1;
			dieSum += dieValue;
		}
		return dieSum;
	}
}















