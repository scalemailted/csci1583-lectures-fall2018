public class StudentPoll
{
	public static void main(String[] args)
	{
		int[] responses = {1,2,3,3,2,5,4,35};
		int[] frequency = new int[6];
		for (int score : responses)
		{
			try
			{
				frequency[score]++;
			}
			catch(ArrayIndexOutOfBoundsException e)
			{
				System.out.println(e);
			}
		}

		for (int i=1; i<frequency.length; i++)
		{
			System.out.printf("%d: %d\n",i,frequency[i]);
		}
	}
}