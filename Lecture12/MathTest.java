public class MathTest
{
	public static void main(String[] args)
	{
		double x = 100.6;
		double y = 3;
		double z = -345;
		double result;

		result = Math.abs(z);
		System.out.printf("abs: %f\n", result);

		result = Math.sqrt(9);
		System.out.printf("sqrt: %f\n", result);

		result = Math.pow(2,3);
		System.out.printf("2**3: %f\n", result);

		result = Math.max(x,y);
		System.out.printf("max: %f\n", result);

		result = MaxGetter.max(x,y,z);
		System.out.printf("maxgetter: %f\n", result);

		result = Math.PI;
		System.out.printf("PI: %.15f\n", result);


	}
}