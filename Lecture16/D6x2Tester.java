public class D6x2Tester
{
	public static void main(String[] args)
	{
		int[] frequency = new int[19];
		for (int i=0; i< 15_000_000; i++)
		{
			int diceSum = Dice.rollD6(3);
			frequency[diceSum]++;
		}

		for (int dieFace=3;dieFace<frequency.length; dieFace++)
		{
			System.out.printf("%d: %d\n", dieFace, frequency[dieFace]);
		}
	}
}