public class PassArray
{
	public static void main(String[] args)
	{
		int[] intArray = {73, 87, 57, 100, 2};
		int[] intArray2 = { 0, 0};
		intArray = intArray2;

		System.out.println(intArray);
		printArray(intArray);
		changeArray(intArray);
		printArray(intArray);

		int x = 100;
		System.out.println("x="+x);
		changeVariable(x);
		System.out.println("x="+x);

	}

	public static void changeVariable(int value)
	{
		value = 0;
	}

	public static void changeArray(int[] array)
	{
		for (int i=0; i<array.length; i++)
		{
			array[i] = 0;
		}
	}

	public static void printArray(int[] array)
	{
		String arrString = "{ ";
		for (int number : array)
		{
			arrString += number + " ";
		}
		arrString += "}";
		System.out.println(arrString);
	}
}