public class Selection2
{
	public static void main(String[] args)
	{
		//multiple seclection
		int option = 5;
		if (option == 1)
		{
			System.out.println("The first option");
		}
		else if (option == 2)
		{
			System.out.println("The second option");
		}
		else
		{
			System.out.println("Try again");
		}
	}
}