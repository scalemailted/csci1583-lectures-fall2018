// 	Summary: Determine a student's average for any number of Labs, Homework, and Tests.
//  Author: Ted Holmberg

import java.util.Scanner;

public class DRYAverager
{

	// Refinement 1:
	public static void main(String[] args)
	{
		// declare a Test average & Get Lab grades from user and sum them and count them
		double testAvg = getAverage("Test");
		// declare a Lab average & Get Homework grades from user and sum them and count them
		double labAvg = getAverage("Lab");
		// declare a Homework average & Get Test grades from user and sum them and count them
		double hwAvg = getAverage("Homework");
		// declare a Final average & Calculate the final average and print it.
		double finalAverage = testAvg * 0.4 + labAvg * 0.1 + hwAvg * 0.5;
		System.out.printf("Final Grade: %.2f\n", finalAverage);
	}

	public static double getAverage(String gradeType)
	{
		// Initialize sum to 0
		double sum = 0;
		// Initialize count to 0
		int count = 0;
		// Initialize Scanner
		Scanner input = new Scanner(System.in);

		//Prompt for grade and get grade
		System.out.printf("Enter %s grades or -1 to quit: ", gradeType);
		double grade = input.nextDouble();

		// Repeat until the grade is less than 0:
		while (grade >= 0)
		{
			// 	add grade to sum
			sum += grade;
			// 	add 1 to count
			count += 1;
			// 	get user input
			grade = input.nextDouble();
		}
		//calculate the average
		double average = sum / count;
		//print the average
		System.out.printf("%s Average is %.2f\n", gradeType, average);
		//return the average
		return average;
	}
}