import java.util.Scanner;
import java.util.InputMismatchException;

public class ScannerTester
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		boolean isCorrect = false;
		while (isCorrect == false)
		{
			try 
			{
				int x = input.nextInt();
				isCorrect = true;
			}
			catch(InputMismatchException e)
			{
				System.out.println("That is not an integer");
				input.nextLine();
			}
		}
	}
}