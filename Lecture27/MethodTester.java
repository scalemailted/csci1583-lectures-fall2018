public class MethodTester
{
	public static void main(String[] args)
	{

	}
}

class X
{
	private int x;

	public X()
	{
		this.x = 0;
	}

	public void fun()
	{
		System.out.println("Hello World");
	}

	public void fun(int i)
	{
		System.out.println("Hello " + i);
	}
}

class EX extends X
{
	public void fun(int i)
	{
		System.out.println("Hello " + i);
	}
}










