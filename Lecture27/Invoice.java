public class Invoice implements Payable
{
	private double cost;
	private String name;
	private String id;

	public Invoice(double cost, String name, String id)
	{
		this.cost = cost;
		this.name = name;
		this.id = id;
	}

	public String toString()
	{
		return String.format("Invoice#: %s, Description: %s, $%.02f",id, name, cost);
	}

	public double getCost()
	{
		return this.cost;
	}
}