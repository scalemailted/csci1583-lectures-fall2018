public class MathException
{
	public static void main(String[] args)
	{
		try
		{
			fun1();
		}
		catch (ArithmeticException e )
		{
			e.printStackTrace();
		}
	}

	public static void fun1()
	{
		fun2();
	}

	public static void fun2()
	{
		int x = 3/0;
	}
}