public class ObjectTester
{
	public static void main(String[] args)
	{
		Point1D x = new Point1D();
		Object[] things = {x, 32, 1.45, true, "Hello", 'g'};
		for (Object i : things)
		{
			System.out.println(i);
		}
	}
}

class Point1D
{
	private int x;

	public String toString()
	{
		return "Point1D";
	}
}