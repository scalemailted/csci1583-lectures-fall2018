public class PaymentSystem
{
	public static void main(String[] args)
	{
		SalaryEmployee tim = new SalaryEmployee("Tim", new Date(1,1,2000),45000);
		CommissionEmployee tonya = new CommissionEmployee("Tonya", new Date(1,1,2000),.25, 10000.48);
		HourlyEmployee tamika = new HourlyEmployee("Tamkia", new Date(1,1,1999), 20, 64.5 );
		Invoice invoice1 = new Invoice(300, "Mops & Brooms", "AC34-6c");
		
		//System.out.printf("%s: $%.02f\n",tim.getName(), tim.getPayment());
		//System.out.printf("%s: $%.02f\n",tonya.getName(), tonya.getPayment());
		//System.out.printf("%s: $%.02f\n",tamika.getName(), tamika.getPayment());
		Payable[] costlyThings = {tim, tonya, tamika, invoice1};

		double sum = 0;
		for (Payable thing: costlyThings)
		{
			printCostlyThing(thing);
			sum += thing.getCost();
		}
		System.out.printf("The total costs is $%.02f\n", sum);
	}

	public static void printCostlyThing(Payable costly)
	{
		System.out.printf("%s: $%.02f\n", costly, costly.getCost() );
	}
}