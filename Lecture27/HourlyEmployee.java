public class HourlyEmployee extends Employee
{
	private double rate;
	private double hours;

	public HourlyEmployee(String name, Date hire, double rate, double hours)
	{
		super(name, hire);
		this.rate = rate;
		this.hours = hours;
	}

	public String toString()
	{
		String employee = super.toString();
		return String.format("[Hourly] %s rate:$%.02f hours:%.01f", employee, this.rate, this.hours);
	}

	public double getPayment()
	{
		return this.rate * this.hours;
	}
}