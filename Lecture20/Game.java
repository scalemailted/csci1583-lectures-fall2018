import java.util.Scanner;

public class Game
{
	private Player player;
	private Scanner input;

	public Game()
	{

		this.input = new Scanner(System.in);
		System.out.print("Enter a name: ");
		String name = input.nextLine();
		this.player = new Player(name);
	}

	public static void main(String[] args)
	{
		Game game = new Game();
		Enemy monster = new Enemy("goblin", 100, 8, 1);
		while (game.player.isAlive() && monster.isAlive() )
		{
			System.out.println(game.player);
			System.out.printf("%s\n\n",monster);
			game.player.attack(monster);
			monster.attack(game.player);
		}
		if (game.player.isAlive() )
		{
			System.out.println("You win!");
		}
		else
		{
			System.out.println("You died!");
		}
	}

}








