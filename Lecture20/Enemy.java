public class Enemy
{
	private String name;
	private int health;
	private int strength;
	private int xp;

	public Enemy(String name, int health, int strength, int xp)
	{
		this.name = name;
		this.health = health;
		this.strength = strength;
		this.xp = xp;
	}

	public String getName()
	{
		return this.name;
	}

	public int getHealth()
	{
		return this.health;
	}

	public int getStrength()
	{
		return this.strength;
	}

	public int getXP()
	{
		return this.xp;
	}

	public void takeDamage(int damage)
	{
		if (this.health >= damage)
		{
			this.health -= damage;
		}
		else
		{
			this.health = 0;
		}
	}

	public void attack(Player hero)
	{
		int damage = (int)(Math.random() * this.strength) + 1;
		hero.takeDamage(damage);
		System.out.printf("%s attacks %s for %d damage\n\n",
							this.name,
							hero.getName(),
							damage);
	}

	public String toString()
	{
		return String.format("[%s] HP:%d, STR:%d", 
								this.name, 
								this.health,
								this.strength);
	}

	public boolean isAlive()
	{
		return this.health > 0;
	}

}