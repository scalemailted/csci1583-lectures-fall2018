public class Player
{
	//Object's instance variables/ properties
	private int health;
	private int strength;
	private int level;
	private String name;

	//constructor
	public Player(String name)
	{
		this.name = name;
		this.health = 100;
		this.strength = 10;
		this.level = 1;
	}

	//Getters 
	public int getHealth()
	{
		return this.health;
	}

	public int getStrength()
	{
		return this.strength;
	}

	public int getLevel()
	{
		return this.level;
	}

	public String getName()
	{
		return this.name;
	}

	//Setters
	public void setHealth(int health)
	{
		if (health >= 0)
		{
			this.health = health;
		}
	}

	public void setStrength(int strength)
	{
		if (strength >= 0)
		{
			this.strength = strength;
		}
	}

	public void setLevel(int level)
	{
		if (level > 0)
		{
			this.level = level;
		}
	}

	public void takeDamage(int damage)
	{
		if (damage < this.health)
		{
			this.health -= damage;
		}
		else
		{
			this.health = 0;
		}
	}

	public void attack(Enemy monster)
	{
		int damage = (int)(Math.random() * this.strength) + 1;
		if ( damage == this.strength)
		{
			damage *= 2;
			System.out.printf("%s crits the %s for %d damage.\n\n",
							this.name,
							monster.getName(),
							damage);

		}
		else
		{
			System.out.printf("%s hits the %s for %d damage.\n\n",
							this.name,
							monster.getName(),
							damage);
		}
		monster.takeDamage(damage);
	}

	public String toString()
	{
		return String.format("[%s] HP:%d, STR:%d, LVL: %d",
								this.name, 
								this.health,
								this.strength,
								this.level);
	}

	public boolean isAlive()
	{
		return this.health > 0;
	}


}