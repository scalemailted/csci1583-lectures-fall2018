import java.util.Scanner;

public class Game
{
	private Player player;
	private Scanner input;

	public Game()
	{

		this.input = new Scanner(System.in);
		System.out.print("Enter a name: ");
		String name = input.nextLine();
		this.player = new Player(name);
	}

	public static void main(String[] args)
	{
		Game game = new Game();
		System.out.println(game.player);
	}

}