public class Player
{
	//Object's instance variables/ properties
	private int health;
	private int strength;
	private int level;
	private String name;

	//constructor
	public Player(String name)
	{
		this.name = name;
		this.health = 100;
		this.strength = 10;
		this.level = 1;
	}

	//Getters 
	public int getHealth()
	{
		return this.health;
	}

	public int getStrength()
	{
		return this.strength;
	}

	public int getLevel()
	{
		return this.level;
	}

	//Setters
	public void setHealth(int health)
	{
		this.health = health;
	}

	public void setStrength(int strength)
	{
		this.strength = strength;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public String toString()
	{
		return String.format("[%s] HP:%d, STR:%d, LVL: %d",
								this.name, 
								this.health,
								this.strength,
								this.level);
	}


}