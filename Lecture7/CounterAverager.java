//Summary: Determine a student's average for 10 quizzes
//Author: Ted Holmberg

import java.util.Scanner;

public class CounterAverager
{
	public static void main(String[] args)
	{
		//Initialize sum to 0
		double sum = 0.0;
		//Initialize count to 0
		int count = 0;
		//Initialize Scanner object for input
		Scanner keyboard = new Scanner(System.in);
		//Declare an average
		double average;


		//repeat until count is 10:
		while (count < 10)
		{
			//Prompt the user for grade
			System.out.print("Enter a grade: ");
			//Get and store the grade from the user
			double grade = keyboard.nextDouble();
			//Add the inputted grade to the sum
			sum += grade;
			//Add one to the count
			count += 1;
		}

		//set the average to the sum / count
		average = sum / count;
		//print average
		System.out.printf("Average: %.2f\n", average);
	}
}