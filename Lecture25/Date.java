public class Date
{
	private static final int[] DAYS_PER_MONTH = 
	{0, 31,28,31,30,31,30,31,31,30,31,30,31};

	private int year;
	private int month;
	private int day;

	public Date(int day, int month, int year)
	{
		this.setDate(day, month, year);
	}

	public void setDate(int day, int month, int year)
	{
		this.setYear(year);
		this.setMonth(month);
		this.setDay(day);
	}

	private void setYear(int year)
	{
		if (year  < 1900)
		{
			throw new IllegalArgumentException("The year must be after 1900");
		}
		this.year = year;
	}

	private void setMonth(int month)
	{
		if ( month >= 13 || month <= 0)
		{
			throw new IllegalArgumentException("The month must be between 1-12");
		}
		this.month = month;
	}

	private void setDay(int day)
	{
		if (day <= 0 || day > DAYS_PER_MONTH[ this.month ] )
		{
			throw new IllegalArgumentException("The day is invalid");
		}

		this.day = day;
	}

	public int getYear()
	{
		return this.year;
	}

	public int getMonth()
	{
		return this.month;
	}

	public int getDay()
	{
		return this.day;
	} 

	public String toString()
	{
		return String.format("%02d/%02d/%d", this.month, this.day, this.year);
	}
}