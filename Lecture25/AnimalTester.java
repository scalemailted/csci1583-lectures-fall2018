public class AnimalTester
{
	public static void main(String[] args)
	{
		Animal[] animals = {new Turtle(), new Wolf(), new Bird() };
		for (Animal a : animals)
		{
			a.move();
			
			if (a instanceof Bird)
			{
				System.out.println("Its a Bird!");
			}
		}
	}
}