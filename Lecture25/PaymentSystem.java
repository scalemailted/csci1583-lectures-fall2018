public class PaymentSystem
{
	public static void main(String[] args)
	{
		SalaryEmployee tim = new SalaryEmployee("Tim", new Date(1,1,2000),45000);
		CommissionEmployee tonya = new CommissionEmployee("Tonya", new Date(1,1,2000),.25, 10000.48);
		HourlyEmployee tamika = new HourlyEmployee("Tamkia", new Date(1,1,1999), 20, 64.5 );
		
		//System.out.printf("%s: $%.02f\n",tim.getName(), tim.getPayment());
		//System.out.printf("%s: $%.02f\n",tonya.getName(), tonya.getPayment());
		//System.out.printf("%s: $%.02f\n",tamika.getName(), tamika.getPayment());
		Employee[] employees = {tim, tonya, tamika};

		for (Employee e: employees)
		{
			System.out.printf("%s $%.02f\n", e.getName(), e.getPayment() );
		}
	}
}