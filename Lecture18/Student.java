public class Student
{
	private String name;
	private int age;
	private double gpa;

	public Student(String name, int age, double gpa)
	{
		this.name = name;
		this.age = age;
		this.gpa = gpa;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	public double getGPA()
	{
		return gpa;
	}

	public void setGPA(double gpa)
	{
		this.gpa = gpa;
	}

	public String toText()
	{
		return name + ", " + age + ", " + gpa;
	}


}