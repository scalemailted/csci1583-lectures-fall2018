public class Student1
{
	private static String name;
	private static int age;
	private static double gpa;

	public static String getName()
	{
		return name;
	}

	public static void setName(String name)
	{
		Student1.name = name;
	}

	public static int getAge()
	{
		return age;
	}

	public static void setAge(int age)
	{
		Student1.age = age;
	}

	public static double getGPA()
	{
		return gpa;
	}

	public static void setGPA(double gpa)
	{
		Student1.gpa = gpa;
	}

	public static String toText()
	{
		return name + ", " + age + ", " + gpa;
	}


}