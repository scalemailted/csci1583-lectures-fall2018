import java.util.ArrayList;

public class ArrayListTester
{
	public static void main(String[] args)
	{
		ArrayList<String> items = new ArrayList<String>();
		displayArray(items);
		items.add("red");
		displayArray(items);
		items.add(0, "yellow");
		displayArray(items);
		items.add(0,"blue");
		displayArray(items);
		items.add(2,"green");
		displayArray(items);

		String color = items.get(1);
		System.out.println("The color at index 1 is: " + color);

		boolean bool = items.contains("blue");
		System.out.println("items contains blue: " + bool);

		bool = items.contains("orange");
		System.out.println("items contains orange: " + bool);

		//enhanced for loop
		for (String col : items)
		{
			System.out.println(col);
		}

		//to Array
		System.out.println(items);
		System.out.println(items.toArray());

		for (int i=0; i<items.size(); i++)
		{
			String col = items.get(i);
			System.out.println(col);
		}

		if (items.contains("red") == true)
		{
			int indexRed = items.indexOf("red");
			items.remove("red");
			items.add(indexRed, "purple");
		}

		System.out.println(items);


	}

	public static void displayArray(ArrayList<String> items)
	{
		System.out.println("size: " + items.size() + " " + items.toString() );
	}
}