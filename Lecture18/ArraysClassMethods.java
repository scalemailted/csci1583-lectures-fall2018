import java.util.Arrays;

public class ArraysClassMethods
{
	public static void main(String[] args)
	{
		int[] array = {2,4,3,5,1};
		int[][] array2d = {{1,2,3},{4,5,6}};
		//default print
		System.out.println(array);
		String text = Arrays.toString(array);
		System.out.println(text);
		text = Arrays.deepToString(array2d);
		System.out.println(text);

		//Sort
		Arrays.sort(array);
		text = Arrays.toString(array);
		System.out.println(text);

		//Fill Array
		int[] fillArray = new int[20];
		System.out.println( Arrays.toString(fillArray) );
		Arrays.fill(fillArray, 7);
		System.out.println( Arrays.toString(fillArray) );

		//Equality
		int[] a = {1,2,4};
		int[] b = {1,2,4};
		int[] c = b;

		boolean bool = (a == b);
		System.out.println("Equality with a == b:" + bool);
		bool = (b == c);
		System.out.println("Equality with b == c:" + bool);

		bool = Arrays.equals(a,b);
		System.out.println("Arrays.equals(a,b):  " + bool);

		//search
		System.out.println( Arrays.toString(array) );
		int index = Arrays.binarySearch(array, 3);
		System.out.println("index of 3: " + index);
		index = Arrays.binarySearch(array, -1);
		System.out.println("index of 6: " + index);

	}
}