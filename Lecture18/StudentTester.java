public class StudentTester
{
	public static void main(String[] args)
	{
		Student1.setName("Tim");
		Student1.setGPA(1.0);
		Student1.setAge(98);
		System.out.println( Student1.toText() );

		Student tim = new Student("tim", 98, 1.0);
		Student kim = new Student("kim", 23, 3.0);
		System.out.println(tim.toText() );
		System.out.println(kim.toText() );

	}
}