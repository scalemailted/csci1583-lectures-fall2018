public class ArraySum
{
	public static void main(String[] args)
	{
		int[] grades = {23, 43, 34, 56, 100};
		int sum = 0;

		for (int score : grades)
		{
			System.out.println(score);
			sum += score;
			score = 0;
		}
		System.out.println("sum2: " + sum);

		sum = 0;
		for (int i=0; i<grades.length; i++)
		{
			sum += grades[i];
		}
		System.out.println("sum1: " + sum);

	}
}