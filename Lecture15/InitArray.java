public class InitArray
{
	public static void main(String[] args)
	{
		String[] groceryArray = new String[6];
		groceryArray[0] = "eggs";
		groceryArray[1] = "milk";
		groceryArray[2] = "chicken";
		groceryArray[3] = "butter";
		groceryArray[4] = "bread";
		groceryArray[5] = "pizza";

		for (int i=0; i<groceryArray.length; i++)
		{
			System.out.printf("%d: %s\n", i, groceryArray[i]);
		}
	}
}