public class InitArray3
{
	public static void main(String[] args)
	{
		String[] groceryArray = {"eggs", "milk", "chicken", "butter", "bread", "pizza"};

		for (int i=0; i<groceryArray.length; i++)
		{
			System.out.printf("%d: %s\n", i, groceryArray[i]);
		}
	}
}