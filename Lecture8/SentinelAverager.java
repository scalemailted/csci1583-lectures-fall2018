/*
Top: Determine a student's average for ANY number of grades
Author: Ted Holmberg
*/

import java.util.Scanner;

public class SentinelAverager
{
	public static void main(String[] args)
	{
		//initialize Scanner for input
		Scanner scanner = new Scanner(System.in);
		//initialize sum to 0
		double sum = 0.0;
		//initialize count to 0
		int count = 0;
		//declare average
		double average;

		//prompt the user for a grade or -1 to exit
		System.out.println("Enter grade or -1 to exit");
		//get and store the grade from the user
		double grade = scanner.nextDouble();

		//repeat until grade is less than 0:
		while (grade >= 0)
		{
			//add grade to the sum
			sum += grade;
			//add one to the count
			count += 1;
		    //get and store the grade from the user
		    grade = scanner.nextDouble();
		}

		//set the average to sum / count
		average = sum / count;
		//print average
		System.out.printf("Average: %.1f\n",average);
	}
}