public class Point3D
{
	private double x;
	private double y;
	private double z;

	public Point3D(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX()
	{
		return this.x;
	}
	public double getY()
	{
		return this.y;
	}
	public double getZ()
	{
		return this.z;
	}

	public void moveTo(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void moveBy(double dx, double dy, double dz)
	{
		this.x += dx;
		this.y += dy;
		this.z += dz;
	}

	public double distance(Point3D other)
	{
		double dx = other.getX() - this.getX();
		double dy = other.getY() - this.getY();
		double dz = other.getZ() - this.getZ();
		double d = Math.sqrt( dx*dx + dy*dy + dz*dz );
		return d; 
	}


}









