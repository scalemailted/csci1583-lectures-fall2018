public static double findSmallest(double x, double y, double z)
{
	double min = x;
	if (min > y)
	{
		min = y;
	}
	if (min > z)
	{
		min = z;
	}
	return min;
}