import java.util.Arrays;

public class PassTester
{
	public static void main(String[] args)
	{

		String[] names = {"Ted", "Tod", "Tad"};
		System.out.println( Arrays.toString(names) );
		reset(names);
		System.out.println( Arrays.toString(names) );
		reset2(names);
		System.out.println( Arrays.toString(names) );
	}

	public static void reset(String[] names)
	{
		names = new String[1];
	}

	public static void reset2(String[] names)
	{
		for (int i=0; i<names.length; i++)
		{
			names[i] = null;
		}
	}

}