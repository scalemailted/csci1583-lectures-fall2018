public class Repetition3
{
	public static void main(String[] args)
	{
		//while loop for counter controlled rep
		int counter1 = 0;
		while (counter1 < 5)
		{
			System.out.printf("%d\t",counter1);
			counter1 += 1;
		}
		System.out.print("\n");

		for(int counter2=0; counter2<5; counter2+=1 )
		{
			System.out.printf("%d\t",counter2);
		}
		System.out.print("\n");

	}
}