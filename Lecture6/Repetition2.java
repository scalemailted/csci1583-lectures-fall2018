public class Repetition2
{
	public static void main(String[] args)
	{
		System.out.println("Before loop");
		int counter = 10;
		while (counter > 0)
		{
			System.out.printf("%d\n",counter);
			//counter = counter + 1;
			counter -= 2;
		}
		System.out.println("\nAfter loop");
	}
}