public class Repetition4
{
	public static void main(String[] args)
	{
		System.out.println("Before loop");
		int counter = 0;
		do
		{
			System.out.printf("%d\t",counter);
			counter += 1;
		}while (counter < 5);
		System.out.printf("\n");
		System.out.println("After loop");
	}
}