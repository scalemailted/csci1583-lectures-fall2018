import java.util.Scanner;

public class Input
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		double text = input.nextDouble();
		System.out.printf("Your input was: $%.2f\n",text+4);
	}
}